/* Group 111
Tim Desch, Konstantin Meudt, Tim Neher, Paul Jakob Weitz

Command to run:
spin smoker_2.pml

Commands to verify with SPIN:
spin -a smoker_2.pml
gcc -o pan pan.c
./pan
*/

#define NUM_SMOKER 2

//pid's of the smoker holding the ressource
//-1 if either of the ressources are available
int paper_id = -1;
int tobacco_id = -1;

active [NUM_SMOKER] proctype Smoker() {
    do
    //check if paper is available
    :: atomic {
        //only grab paper if tobacco isn't held by another smoker
        (paper_id == -1 && (tobacco_id == -1 || tobacco_id == _pid)) ->
            paper_id = _pid;//acquire paper
            printf("Smoker %d acquires paper\n",_pid);
    }
    //check if tobacco and is available
    :: atomic {
        //only grab tobacco if paper isn't held by another smoker
        (tobacco_id == -1 && (paper_id == -1 || paper_id == _pid)) ->
            tobacco_id = _pid;//acquire tobacco
            printf("Smoker %d acquires tobacco\n",_pid);
    }
    //if smoker holds both ressources, smoke and release ressources
    :: atomic {
        (paper_id == tobacco_id) && (tobacco_id == _pid) ->
            printf("Smoker %d is smoking\n",_pid);
            paper_id = -1;
            tobacco_id = -1;
    }
    od
}

//deadlocks occour if both ressources are held by different smokers
#define both_taken ((paper_id != -1) && (tobacco_id != -1))
#define different_smokers (paper_id != tobacco_id)

//never claim to prevent deadlock if both smokers hold one ressource
never {
    do
    :: both_taken && different_smokers -> break
    :: else -> skip
    od
}
