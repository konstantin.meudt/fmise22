/* Group 111
Tim Desch, Konstantin Meudt, Tim Neher, Paul Jakob Weitz

Command to run:
spin bridgetorch.pml

Commands to disprove with SPIN:
spin -a bridgetorch.pml
gcc -o pan pan.c
./pan -a
spin -t bridgetorch.pml
*/


// Persons a, b, c and d, true represents the person being at the bus stop 
bool a = false;
bool b = false;
bool c = false;
bool d = false;
int t = 0;


proctype start_station(){
    printf("a: %d | b: %d | c: %d | d: %d | Time: %d\n",a, b, c, d, t);
    // Decide which group of 2 will go over the bridge with the torch
    if
        :: !a && !b -> t = t + 2; printf("Person A and B walk to the bus station.\n"); a = true; b = true
        :: !a && !c -> t = t + 5; printf("Person A and C walk to the bus station.\n"); a = true; c = true
        :: !a && !d -> t = t + 8; printf("Person A and D walk to the bus station.\n"); a = true; d = true
        :: !b && !c -> t = t + 5; printf("Person B and C walk to the bus station.\n"); b = true; c = true
        :: !b && !d -> t = t + 8; printf("Person B and D walk to the bus station.\n"); b = true; d = true
        :: !c && !d -> t = t + 8; printf("Person C and D walk to the bus station.\n"); c = true; d = true
    fi

    // Check if the program terminates: All persons are at the bus station or the time is over 15 minutes
    // else: The method bus_station gets called
    if
        :: a && b && c && d -> run terminate()
        :: t > 15 -> run terminate()
        :: else -> run bus_station()
    fi
}


proctype bus_station(){
    printf("a: %d | b: %d | c: %d | d: %d | Time: %d\n",a, b, c, d, t);
    // Decide which person goes back over the bridge with the torch
    if
        :: a -> t = t + 1; printf("Person A goes back.\n"); a = false
        :: b -> t = t + 2; printf("Person B goes back.\n"); b = false
        :: c -> t = t + 5; printf("Person C goes back.\n"); c = false
        :: d -> t = t + 8; printf("Person D goes back.\n"); d = false
    fi

    // Check if the program terminates: Time is over 15 minutes
    // else: The method start_station gets called
    if
        :: t > 15 -> run terminate()
        :: else -> run start_station()
    fi
}


proctype terminate(){
    printf("a: %d | b: %d | c: %d | d: %d\n", a, b, c, d);
    printf("Time: %d\n", t);
    if
        :: t <= 15 -> printf("Everyone got to the bus within 15 minutes!\n");
        :: else -> printf("Someone missed the bus.\n");
    fi
}


init{
    // Initialize the bridgetorch problem with all persons being at the start station and the time t is 0
    run start_station();
}


// LTL formula to disprove
ltl too_late {[] !(a && b && c && d && (t<=15))}
