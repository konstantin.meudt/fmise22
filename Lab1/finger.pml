/* Group 111
Tim Desch, Konstantin Meudt, Tim Neher, Paul Jakob Weitz

Command to run:
spin finger.pml

Commands to verify with SPIN:

Disprove LTL formula win_not_possible_for_alice:
spin -a finger.pml
gcc -o pan pan.c
./pan -a -N win_not_possible_for_alice
spin -t finger.pml

Disprove LTL formula win_not_possible_for_bob:
./pan -a -N win_not_possible_for_bob
spin -t finger.pml

Verify LTL formula never_all_4:
./pan -a -N never_all_4
*/


#define N 5

// Represents the left and right hand of alice
byte aliceHands[2];
// Represents the left and right hand of bob
byte bobsHands[2];

bool finished = false;
bool nextTurn = true;
bool aliceWon = false;
bool bobWon = false;
bit turn = 0;


proctype Alice(){
    bit i;
    bit k;
    byte old;

    // Select available hand for Alice randomly (0 fingers represents that this hand is disqualified)
    if
        :: aliceHands[0]!=0 -> i=0
        :: aliceHands[1]!=0 -> i=1
        :: else -> bobWon=true; finished=true; printf("Alice lost, Bob won.\n"); goto Fin1
    fi
    // Select available hand for Bob randomly
    if
        :: bobsHands[0]!=0 -> k=0
        :: bobsHands[1]!=0 -> k=1
    fi
    old = bobsHands[k];

    // Make the chosen play
    bobsHands[k] = (bobsHands[k]+aliceHands[i]) % N;

    printf("Alice chose her hand with index %d (%d fingers) and Bobs hand with index %d (%d fingers). Bobs hand with index %d now shows %d fingers.\n",
            i, aliceHands[i], k, old, k, bobsHands[k]);
    printf("------------------------------------\n");

    nextTurn=true;

    Fin1:
}


proctype Bob(){
    bit i;
    bit k;
    byte old;

    // Select available hand for Bob randomly (0 fingers represents that this hand is disqualified)
    if
        :: bobsHands[0]!=0 -> i=0
        :: bobsHands[1]!=0 -> i=1
        :: else -> aliceWon=true; finished=true; printf("Bob lost, Alice won.\n"); goto Fin2
    fi
    // Select available hand for Alice randomly
    if
        :: aliceHands[0]!=0 -> k=0
        :: aliceHands[1]!=0 -> k=1
    fi
    old = aliceHands[k];

    // Make the chosen play
    aliceHands[k] = (aliceHands[k]+bobsHands[i]) % N;

    printf("Bob chose his hand with index %d (%d fingers) and Alices hand with index %d (%d fingers). Alices hand with index %d now shows %d fingers.\n",
            i, bobsHands[i], k, old, k, aliceHands[k]);
    printf("------------------------------------\n");

    nextTurn=true;

    Fin2:
}


init{
    // Initialize all hands with 1 finger
    aliceHands[0] = 1;
    aliceHands[1] = 1;
    bobsHands[0] = 1;
    bobsHands[1] = 1;

    // Play the game, with players taking turns
    do
        :: (turn==0 && !finished && nextTurn) -> nextTurn=false; turn=1; run Alice()
        :: (turn==1 && !finished && nextTurn) -> nextTurn=false; turn=0; run Bob()
        :: finished -> break
    od
}


// LTL formulas to disprove
ltl win_not_possible_for_alice {[]!aliceWon} 
ltl win_not_possible_for_bob {[]!bobWon}

// LTL formula to verify for maximum search depth
ltl never_all_4 {[] !(aliceHands[0]==4 && aliceHands[1]==4 && bobsHands[0]==4 && bobsHands[1]==4)}
