/* Group 111
Tim Desch, Konstantin Meudt, Tim Neher, Paul Jakob Weitz

Command to run:
spin parity.pml

Commands to verify with SPIN:
spin -a parity.pml
gcc -o pan pan.c
./pan -a
*/


chan stom = [0] of {bit, bit, bit, bit, bit, bit, bit, bit}; /*initalize channels for exchange of messages */
chan mtor = [0] of {bit, bit, bit, bit, bit, bit, bit, bit};
bool arraymanipulated = false;
bool manipulationdetected = false;


active proctype Sender(){
    bit arr [8]; //create array of bits of length 8
    int i = 0; //index
    bit parityBit = 0;
    bit k;

    // Initialize array with random bit values
    for (i: 0 .. 6){
        select(k: 0 .. 1);
        arr[i] = k;
        parityBit = (parityBit+arr[i]) % 2;
    }
    arr[7] = parityBit;

    for(i : 0 .. 7){
        printf("arr[%d] = %d\n", i, arr[i]); //print the array
    }
    printf("------------------------------------\n");

    // Send array to MitM
    stom ! arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7];
}


active proctype MITM(){
    bit brr[8];
    int i;
    int bitToFlip = 0;

    // Receive array
    stom ? brr[0], brr[1], brr[2], brr[3], brr[4], brr[5], brr[6], brr[7];
    
    // Flip a bit or not, 50/50
    if
        :: select(i: 0 .. 7);  // Flip a random bit
        brr[i] = (brr[i]+1) % 2;
        arraymanipulated = true;
        printf("Flipped bit at index %d.\n", i)
        :: printf("No bit was flipped.\n")
    fi

    for(i : 0 .. 7){
        printf("brr[%d] = %d\n", i, brr[i]); //print array
    }
    printf("------------------------------------\n");

    // Send array to receiver
    mtor ! brr[0], brr[1], brr[2], brr[3], brr[4], brr[5], brr[6], brr[7];
}


active proctype Receiver(){
    bit crr[8];
    bit parity = 0;
    int i;

    // Receive array
    mtor ? crr[0], crr[1], crr[2], crr[3], crr[4], crr[5], crr[6], crr[7];

    // Check parity
    for (i: 0 .. 6){
        parity = (parity+crr[i]) % 2;
    }
    if
        :: crr[7]!=parity -> manipulationdetected = true; printf("Output: %d. The array has been manipulated.\n", 1)
        :: else -> printf("Output: %d. The array has not been manipulated.\n", 0)
    fi
}


ltl always_detected {[](arraymanipulated -> <>manipulationdetected) && (manipulationdetected -> arraymanipulated)}
