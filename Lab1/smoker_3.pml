/* Group 111
Tim Desch, Konstantin Meudt, Tim Neher, Paul Jakob Weitz

Command to run:
spin smoker_3.pml

Commands to verify with SPIN:
spin -a smoker_3.pml
gcc -o pan pan.c
./pan -a
*/

//if #NUM_SMOKER + 1 ressources exist, no deadlock occours
#define NUM_SMOKER 8
#define NUM_TOBACCO NUM_SMOKER
#define NUM_PAPER 1

//count how much ressources are available
int paper_available = NUM_PAPER;
int tobacco_available = NUM_TOBACCO;

//tracks which smoker holds which ressource
bool holds_paper [NUM_SMOKER];
bool holds_tobacco [NUM_SMOKER];

active [NUM_SMOKER] proctype Smoker() {
    do
    //check if paper is available
    :: atomic {
        (paper_available > 0 && !holds_paper[_pid]) ->
            holds_paper[_pid] = true;//acquire paper
            paper_available--;
            getPaper:
            printf("Smoker %d acquires paper\n",_pid);
    }
    //check if tobacco is available
    :: atomic {
        (tobacco_available > 0 && !holds_tobacco[_pid]) ->
            holds_tobacco[_pid] = true;//acquire tobacco
            tobacco_available--;
            getTobacco:
            printf("Smoker %d acquires tobacco\n",_pid);
    }
    //if smoker holds both ressources, smoke and release ressources
    :: atomic {
        (holds_paper[_pid] && holds_tobacco[_pid]) ->
            smoke:
            printf("Smoker %d is smoking\n",_pid);
            holds_paper[_pid] = false;
            paper_available++;
            holds_tobacco[_pid] = false;
            tobacco_available++;
    }
    od
}


//ltl to prevent deadlock if from one point onwards either ressource isn't available
#define no_paper (!<>[](paper_available == 0))
#define no_tobacco (!<>[](tobacco_available == 0))
ltl no_deadlock { no_paper || no_tobacco }
