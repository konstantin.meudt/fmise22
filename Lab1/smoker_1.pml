/* Group 111
Tim Desch, Konstantin Meudt, Tim Neher, Paul Jakob Weitz

Command to run:
spin smoker_1.pml
*/

#define NUM_SMOKER 2

//pid's of the smoker holding the ressource
//-1 if either of the ressources are available
int paper_id = -1;
int tobacco_id = -1;

active [NUM_SMOKER] proctype Smoker() {
    do
    //check if paper is available
    :: atomic {
        (paper_id == -1) ->
            paper_id = _pid;//acquire paper
            getPaper:
            printf("Smoker %d acquires paper\n",_pid);
    }
    //check if tobacco is available
    :: atomic {
        (tobacco_id == -1) ->
            tobacco_id = _pid;//acquire tobacco
            getTobacco:
            printf("Smoker %d acquires tobacco\n",_pid);
    }
    //if smoker holds both ressources, smoke and release ressources
    :: atomic {
        (paper_id == tobacco_id) && (tobacco_id == _pid) ->
            smoke:
            printf("Smoker %d is smoking\n",_pid);
            paper_id = -1;
            tobacco_id = -1;
    }
    od
}
