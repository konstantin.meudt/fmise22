/*
Group 111
Tim Desch, Konstantin Meudt, Tim Neher, Paul Jakob Weitz
*/

public class images {

	//1.
	/*@ public instance invariant (height == image.length) && (height > 0) && (width == image[0].length) && (width > 0);
	@*/
	private /*@ spec_public @*/ int height;
	private /*@ spec_public @*/ int width;
	private /*@ spec_public @*/ int[][] image;
	
	private /*@ spec_public @*/ int[] coordinates;
	

	public images(int[][] image) {
		throw new RuntimeException("No implementation given");
	}


	//2.
	/*@ public normal_behavior
	 @ requires (row >= 0 && row < height && column >= 0 && column < width);
	 @ assignable image[row][column] == newValue;
	 @ ensures	(\forall int i, j; i >= 0 && i < height && j >= 0 && j < width && !(i == row && j == column); \old(image[i][j]) == image[i][j]);
	 @ ensures	image[row][column] == newValue;
	 @ ensures \result == \old(image[row][column]);
	 @
	 @ also
	 @
	 @ exceptional_behavior
	 @ requires !(row >= 0 && row < height && column >= 0 && column < width);
	 @ assignable \nothing;
	 @ signals_only ArrayIndexOutOfBoundsException;
	 @ signals (ArrayIndexOutOfBoundsException) \old(image) == image && (\forall int i, j; i >= 0 && i < height && j >= 0 && j < width; \old(image[i][j]) == image[i][j]);
	 @*/
	public int replacePixel(int row, int column, int newValue) {
    	throw new RuntimeException("No implementation given");
	}


	//3. ("assignable image[*][*]" is not valid syntax, hence "assignable \everything" and the additional ensures)
	/*@ public normal_behavior
	@ assignable \everything;
	@ ensures height == \old(height) && width == \old(width) && image == \old(image) && coordinates == \old(coordinates);
	@ ensures (\forall int i; i >= 0 && i < height; image[i] = \old(image[i]);
	@ ensures (\forall int i; i >= 0 && i < coordinates.length; coordinates[i] == \old(coordinates[i]);
	@ ensures (\forall int i, j; i >= 0 && i < height && j >= 0 && j < width; image[i][j] == \old(image[height-1-i][width-1-j]);
	@*/
    public void mirrorImage() {
    	throw new RuntimeException("No implementation given");
    }


    //4.
	/*@ public normal_behavior
	 @ requires startRow >= 0 && startRow <= endRow && endRow < height && startColumn >= 0 && startColumn <= endColumn && endColumn < width;
	 @ assignable image, image[*];
	 @ ensures (\forall int i, j; i >= 0 && i < height && j >= 0 && j < width; image[i][j] == \old(image[startRow+i][startColumn+j]));
	 @
	 @ also
	 @
	 @ exceptional_behavior
	 @ requires !(startRow >= 0 && startRow <= endRow && endRow < height && startColumn >= 0 && startColumn <= endColumn && endColumn < width);
	 @ assignable coordinates, coordinates[*];
	 @ signals_only ArrayIndexOutOfBoundsException;
	 @ signals (ArrayIndexOutOfBoundsException) coordinates[0] == startRow && coordinates[1] == startColumn && coordinates[2] == endRow && coordinates[3] == endColumn;
	 @*/
    public void minimizeImage(int startRow, int startColumn, int endRow, int endColumn) {
    	throw new RuntimeException("No implementation given");
    }


    //5.
	/*@ public normal_behavior
	@ assignable \nothing;
	@ ensures (\forall int i, j; i >= 0 && i < height && j >= 0 && j < width && image[i][j] >= threshold; \result[i][j] == image[i][j]);
	@ ensures (\forall int i, j; i >= 0 && i < height && j >= 0 && j < width && image[i][j] < threshold; \result[i][j] == 0);
	@*/
    public int[][] thresholdFilter(int threshold) {
    	int[][] newImage = new int[height][width];
    	for (int i = 0; i < height; i++) {
    		for (int j = 0; j < width; j++) {
    			if (image[i][j] < threshold) newImage[i][j] = 0;
    			else newImage[i][j] = image[i][j];
    		}
    	}
    	return newImage;
    }


    //6.
	/*@ public normal_behavior
	@ assignable image;
	@ ensures (\forall int i, j; i >= 0 && i < height && j >= 0 && j < width;
	@ (\exists int k, l; k >= -1 && k <= 1 && l >= -1 && l <= 1 && i+k >= 0 && i+k < height && j+l >= 0 && j+l < width;
	@ image[i][j] == \old(image[i+k][j+l]) &&
	@ (\forall int m, n; m >= -1 && m <= 1 && n >= -1 && n <= 1 && i+m >= 0 && i+m < height && j+n >= 0 && j+n < width;
	@ image[i][j] >= \old(image[i+m][j+n]))));
	@*/
    public void replaceHighestNeighbour() {
	    int[][] newImage = new int[height][width];
	    for (int i = 0; i < height; i++) {
	    	for (int j = 0; j < width; j++) {
	    		int highestValue = 0;
	    		for (int k = 0; k < 3; k++) {
	    			for (int l = 0; l < 3; l++) {
	    				int x = i + (k - 1);
	    				int y = j + (l - 1);
	    				if (x >= 0 && x < height && y >= 0 && y < width) 
	    					if (highestValue < image[x][y]) highestValue = image[x][y];
	    			}
	    		}
	    		newImage[i][j] = highestValue;
	    	}
	    }
	    image = newImage;
    }
	
}
