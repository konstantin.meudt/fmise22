/*
Group 111
Tim Desch, Konstantin Meudt, Tim Neher, Paul Jakob Weitz
*/

public class VendingMachine {
	
	/*
	 * A vending machine has N item slots.
	 */
	// 4.2: #Items == #ItemNames and N
	/*@ public invariant
	  @ (items.length == itemNames.length && items.length == N);
	  @*/
	private /*@spec_public */ final int N = 3;


	/*
	 * The name of an item in a slot is given by the String in the same index in this array, e.g. slot 0 stores Water.
	 */
	private /*@spec_public */ final String[] itemNames = new String[] {"Water", "Lemonade", "Chocolate"};
	
	/*
	 * This array stores for each item slot the quantity of items in that slot. 
	 */
	// 4.1: all entries in items must be positive
	/*@ public invariant
	  @ (\forall int i ; 0 <= i && i < N;
	  @					items[i] >= 0);
	  @*/
	private /*@spec_public */ int[] items = new int[N];
	
	/*
	 * This boolean variable represents iff the vending machine has to be refilled.
	 */
	private /*@spec_public */ boolean needsRefill = false;
	
	
	
	/* 
	 * True iff the item slot 'itemSlot' exists.
	 */
	public /*@ pure @*/ boolean itemSlotExists(int itemSlot) {
		return 0 <= itemSlot && itemSlot < N;
	}
	
	/* 
	 * True iff the item slot 'itemSlot' exists and the item is in stock.
	 */
	public /*@ pure @*/ boolean itemIsInStock(int itemSlot) {
		return itemSlotExists(itemSlot) && items[itemSlot] > 0;
	}
	
	
	//4.3:
	/*@ public normal_behavior
	  @ requires itemIsInStock(item);
	  @ assignable items[item];
	  @ ensures items[item] == \old(items[item] - 1);
	  @ also
	  @ 
	  @ public exceptional_behavior 
	  @ requires !itemSlotExists(item);
	  @ signals_only ItemNotFoundException;
	  @ signals (ItemNotFoundException) items[item] == \old(items[item]);
	  @ also
	  @ 
	  @ public exceptional_behavior 
	  @ requires itemSlotExists(item) && !itemIsInStock(item);
	  @ signals_only ItemEmptyException;
	  @ signals (ItemEmptyException) items[item] == \old(items[item]);
	  @ 
	  @ */
	public String buy(int item) throws RuntimeException {
		if (!itemSlotExists(item)) {
			throw new ItemNotFoundException();
		} else if (!itemIsInStock(item)) {
			needsRefill = true;
			throw new ItemEmptyException();
		}
		
		items[item]--;
		return itemNames[item];
	}
	
	
	
	/*@ public normal_behavior
	  @ requires !needsRefill;
	  @ assignable \nothing;
	  @
	  @ also
	  @
	  @ public normal_behavior
	  @ requires needsRefill;
	  @ assignable items[*], needsRefill;
	  @ ensures (\forall int x; 0 <= x && x < N; \old(items[x]) == 0 ? items[x] == (\old(items[x]) + 3) : items[x] == \old(items[x]));
	  @ ensures !needsRefill;
	  @ */
	// 4.4:
	public void refill() {
		if(needsRefill) {
		  /*@ loop_invariant 0 <= i && i <= N &&
    	  	@ (\forall int j; 0 <= j && j < i; \old(items[j]) == 0 ? items[j] == (\old(items[j]) + 3) : items[j] == \old(items[j])) && (\forall int j; j >= i && j < N; items[j] == \old(items[j]));
    	  	@ assignable items[*];
    	  	@ decreases N - i;
    	  	@*/
			for(int i = 0; i < N; i++) {
				if(items[i] == 0) {
					items[i] = items[i] + 3;
				}
			}
			needsRefill = !needsRefill;
		}
	}
}
