/*
Group 111
Tim Desch, Konstantin Meudt, Tim Neher, Paul Jakob Weitz
*/

public class proof_analysis {
	
	/*@ public invariant next.previous == this && previous.next == this; */
	private /*@ spec_public @*/ proof_analysis next;
	private /*@ spec_public @*/ proof_analysis previous;
	
    /*@ public invariant 0 <= lookup && lookup < values.length; */
    private /*@ spec_public @*/ int[] values;
    private /*@ spec_public @*/ int lookup;
    
    
    /*@ public normal_behaviour
      @ assignable previous, predecessor.next;
      @ ensures previous == predecessor && predecessor.next == this;
      @*/
    public void changePrev(proof_analysis predecessor) {
    	previous = predecessor;
    	predecessor.next = this;
    }
    
    
    /*@ public normal_behaviour
      @ requires 0 <= index && index < values.length;
      @ assignable values[index];
      @ ensures values[index] == element;
      @*/
    public boolean set(int element, int index) {
    	if (index < 0 || index >= values.length) return false;
    	values[index] = element;
    	return true;
    }
    
    /*@ public normal_behaviour
      @ requires 0 <= i && 0 <= j && i < values.length && j < values.length;
      @ assignable values[i], values[j];
      @ ensures values.length == \old(values.length);
      @ ensures values[i] == \old(values[j]) && values[j] == \old(values[i]);
      @ ensures \result;
      @*/
    public boolean swapValues(int i, int j) {
    	if (i < 0 || j < 0 || i >= values.length || j >= values.length) throw new ArrayIndexOutOfBoundsException();
    	int a = values[i];
    	int b = values[j];
    	return set(a, j) && set(b, i); 
    }
    
    
    /*@ public normal_behavior
      @ assignable values[*], lookup;
      @ ensures \old(values[lookup]) == values[lookup];
      @ ensures \old(values[lookup]) != \old(values)[\old(lookup)];
      @*/
    public void action() {
       values[lookup - 1] = values[lookup];
       values[lookup] = values[lookup] + 1;
       lookup--;
    }
    
    
    
    
    
    /* ----------------------------------FRAGEBOGEN-----------------------------------
     * 
     * -------------------------------------------------------------------------------
     * Frage 1: Warum erf�llt die Methode changePrev(predecessor) ihre Spezifikation nicht? 
     * -------------------------------------------------------------------------------
     * Antwort 1: Wenn predecessor!=this und self.next==this, kann Key den Beweis nicht abschließen.
     Wenn self.next==this ist, muss daraus logisch auch self.previous==this gelten, tut es im Fall
     predecessor!=this aber nicht. Wegen diesem Fall können die Nachbedingungen nicht garantiert werden.
     *
     * -------------------------------------------------------------------------------
     * Frage 2: Versuchen Sie die Methode swapValues(int i, int j) unter Method 
     *    Treatment 'Contract' und 'Expansion' zu verifizieren. Unter welchem
     *    Treatment kann KeY den Beweis schlie�en? Falls KeY den Beweis nicht
     *    schlie�en kann, warum?
     * -------------------------------------------------------------------------------
     * Antwort 2: Method Treatment Contract funktioniert nicht.
     Method Treatment Expand funktioniert dagegen.
     Dass liegt daran dass nicht \result==true gelten muss. Wenn der Contract überprüft wird, schlägt diese
     Nachbedingung fehl.
     * 
     * -------------------------------------------------------------------------------
     * Frage 3: Warum erf�llt die Methode action() ihre Spezifikation nicht?
     * -------------------------------------------------------------------------------
     * Antwort 3: Die Methode action() erfüllt ihre Spezifikation nicht, da das lookup-1 
        bei Java zu einer ArrayOutOfboundsException führen kann. Die einzige Einschränkung für lookup
        ist die Instanzinvariante "0 <= lookup < values.length". Der Proof Tree hat ein Open Goal
        im Fall "!(lookup>=1)", da hier "lookup=0" gewählt werden kann und es so zu einer
        ArrayOutOfboundsException kommen kann. Die Nachbedingungen können in diesem Fall also
        nicht garantiert werden, da es zu keiner Änderung des values-Arrays kommt.
     * 
     */
    
    
    
    /*@ public normal_behavior
      @ ensures \old(values[lookup]) == values[lookup];
      @ ensures 2 * \old(values[lookup]) == \old(values)[\old(lookup)] - lookup;
      @*/
    public void action2() {
        throw new RuntimeException("Not yet implemented");
    }
    
}

